from __future__ import print_function


from time import time
from kivy.app import App
from os.path import dirname, join
from os import environ
import os
from kivy.lang import Builder
from kivy.properties import ObjectProperty
# from kivy.clock import Clock
from kivy.uix.tabbedpanel import TabbedPanel
# from kivy.animation import Animation
# from kivy.uix.screenmanager import Screen
from kivy.core.window import Window
from kivy.event import EventDispatcher
from MidiInput import MidiInput
import mido
import panels
import logging
import sys
from lib import AudioEngine
from panels import FileBrowserPanel, SamplePanel

app = None



class TabbedNavigation(TabbedPanel):
    def __init__(self, **kwargs):
        # make sure we aren't overriding any important functionality
        super(TabbedNavigation, self).__init__(**kwargs)
        fbrowserPanel = FileBrowserPanel(**kwargs)
        self.add_widget(fbrowserPanel)
        samplePanel = SamplePanel(**kwargs)
        self.add_widget(samplePanel)

        global app
        app.bind(midiMessage=self.on_midi_tab_change);
    def on_midi_tab_change(self, instance, message):
        print(message)
        if message.type == 'pitchwheel':
            i = self.tab_list.index(self.current_tab)
            if message.pitch == 8191:
                print('next tab')
                self.switch_to(self.tab_list[i - 1 % len(self.tab_list)], do_scroll=True)
            if message.pitch == -8192:
                self.switch_to(self.tab_list[i + 1 % len(self.tab_list)], do_scroll=True)
                print('prev tab')

class PiSamplerApp(App):
    midiMessage = ObjectProperty(None, allownone=True, force_dispatch=True)
    audio = ObjectProperty(None)
    def __init__(self):
        super(PiSamplerApp, self).__init__()
        # port = sys.argv[1] if len(sys.argv) > 1 else None
        self.audio = AudioEngine(True)

        for port in mido.get_input_names():
            if "MPKmini2" not in port:
                continue
            print("Found MIDI Input: ", port)
            try:
                self.midiin = mido.open_input(port, callback=self.on_midi)
            except (EOFError, KeyboardInterrupt):
                sys.exit()

    def on_midi(self, message):
        self.midiMessage = message
        print(message)

    def build(self):
        return TabbedNavigation()
if __name__ == '__main__':
    app = PiSamplerApp()
    print(app.user_data_dir)
    app.run()
