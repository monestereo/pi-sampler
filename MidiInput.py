from kivy.uix.widget import Widget
from kivy.properties import ListProperty

class MidiInput(Widget):
        
    def __init__(self):
        super(MidiInput, self).__init__()
        print("init input handler")
        #self._wallclock = time.time()

    def on_midi(self, event, data=None):
        global app
        message, deltatime = event
        #self._wallclock += deltatime
        print("[%s] %r" % (self.port, message))
        self.message = message
