from kivy.uix.tabbedpanel import TabbedPanelItem
from kivy.lang import Builder
from os import path
#from kivy.uix.filechooser import FileChooserListView
from kivy.properties import StringProperty, ObjectProperty, NumericProperty
from kivy.app import App
from kivy.core.window import Window

from components import FileBrowser

Builder.load_string('''
#:kivy 1.2.0
<FileBrowserPanel>:
    text: 'Files'
    filechooser: filechooser
    BoxLayout:
        size: root.size
        pos: root.pos
        orientation: "vertical"
        FileBrowser:
            id: filechooser
            rootpath: root.user_path
        BoxLayout:
            size_hint_y: None
            height: 50
            id: wave_display
        BoxLayout:
            size_hint_y: None
            height: 30
            id: bottom_bar
            Button:
                text: "Slice"
                on_release: root.on_slice(filechooser.path, filechooser.selection)
''')
class FileBrowserPanel(TabbedPanelItem):
    user_path = StringProperty(None)
    current_index = NumericProperty(0)
    filechooser = ObjectProperty(None)
    audio = ObjectProperty(None)
    def __init__(self, **kwargs):
        super(FileBrowserPanel, self).__init__(**kwargs)
        self.audio = App.get_running_app().audio
        self.audio.bind(on_sound_load=self.on_sload)

    def on_sload(self, event, sound):
        waveform_widget = self.audio.get_waveform(sound)
        self.ids.wave_display.add_widget(waveform_widget)

    def _on_select(self, selection):
        print('selection: ', selection)

    def on_slice(self, path, files):
        print('slice: ', files[0])
