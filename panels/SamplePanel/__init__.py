from kivy.uix.tabbedpanel import TabbedPanelItem
from kivy.lang import Builder
from os import path
# from kivy.garden.filebrowser import FileBrowser
# from kivy.properties import StringProperty
from kivy.app import App

Builder.load_string('''
<SamplePanel>:
    text: 'Sample'
    BoxLayout:
        size: root.size
        pos: root.pos
        orientation: "vertical"
        Button:
            text: "Test"
''')

class SamplePanel(TabbedPanelItem):
    def __init__(self, **kwargs):
        super(TabbedPanelItem, self).__init__(**kwargs)
