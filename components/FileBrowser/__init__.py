from kivy.uix.boxlayout import BoxLayout
from kivy.uix.treeview import TreeViewLabel, TreeView
from kivy.uix.button import Button

from kivy.properties import (ObjectProperty, StringProperty, OptionProperty,
                             ListProperty, BooleanProperty, NumericProperty, StringProperty)
from kivy.lang import Builder
from kivy.utils import platform
from kivy.clock import Clock
from kivy.compat import PY2
import string
from os.path import sep, dirname, expanduser, isdir, join
import os
try:
    from os import scandir
except ImportError:
    from scandir import scandir  # use scandir PyPI module on Python < 3.5

from sys import getfilesystemencoding
from functools import partial
from kivy.app import App
from kivy.modules import inspector
from kivy.core.window import Window
from kivy.adapters.listadapter import ListAdapter
from kivy.uix.listview import ListItemButton, ListView

SUPPORTED_FILES = ['mp3', 'wav']

Builder.load_string('''
#:kivy 1.2.0
#:import metrics kivy.metrics
#:import abspath os.path.abspath

<FileBrowser>:
    orientation: 'vertical'
    spacing: 5
    padding: [6, 6, 6, 6]
    BoxLayout:
        orientation: 'vertical'
        spacing: 0
        id: container
''')

class CustomListItemButton(ListItemButton):
    file = ObjectProperty(None)
    pass

class VirtualRootEntry(object):
    name = '..'
    is_root = True
    path = None
    def is_dir(self):
        return True
    def __init__(self, p):
        self.path = p

class FileBrowser(BoxLayout):
    path = StringProperty(None)
    items = ListProperty([])
    selection = NumericProperty(0)
    root_path = StringProperty(None)
    #tv = ObjectProperty()
    list_adapter = None
    list_view = None
    def __init__(self, **kwargs):
        super(FileBrowser, self).__init__(**kwargs)
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        Clock.schedule_once(self.load)

    def load(self, *l):
        user_path = App.get_running_app().user_data_dir
        self.root_path = user_path
        self.path = user_path

    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard = None

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        print('self items: ', keycode[1])
        if keycode[1] == 'backspace':
            if self.path != self.root_path:
                head, tail = os.path.split(self.path)
                self.path = head

        if keycode[1] == 'enter':
            if self.list_adapter.get_view(self.selection):
                _f = self.list_adapter.get_view(self.selection).file

                if _f.is_dir():
                    if (self.path in _f.path) and self.path != _f.path:
                        self.path = _f.path
                    else:
                        self.path = _f.path
                elif _f.is_file():
                    if _f.name.split(".")[-1] in SUPPORTED_FILES:
                        App.get_running_app().audio.play(_f.path)
        elif keycode[1] == 'up':
            if len(self.list_adapter.data) > 0:
                #self.selection = (self.selection - 1) % len(self.list_adapter.data)
                if self.selection - 1 < 0:
                    self.selection = len(self.list_adapter.data) - 1
                else:
                    self.selection = self.selection - 1
        elif keycode[1] == 'down':
            if len(self.list_adapter.data) > 0:
                if self.selection + 1 > (len(self.list_adapter.data) - 1):
                    self.selection = 0
                else:
                    self.selection = self.selection + 1

    #def selection_changed(self, adapter):
        # print('ARGS: ', adapter.selection)
        # if adapter.selection.length():
        #     self.list_view.children[0].scroll_to(adapter.selection, padding = 30, animate=False)

    def on_selection(self, instance, value):
        # # if self.selection > self.displayable:
        # #     print('AMK _ Normally:', len(self.items) - self.displayable)
        # #     scroll_to_view = self.list_adapter.get_view(self.selection)
        # #     if self.selection > len(self.items) - self.displayable:
        # #         print('AMK _ End:', len(self.items) - self.displayable + 1)
        # #         scroll_to_view = self.list_adapter.get_view(len(self.items) - self.displayable + 1)
        # #     if self.selection < self.displayable:
        # #         print('AMK _ Start:', len(self.items) - self.displayable)
        # #         scroll_to_view = self.list_adapter.get_view(0)
        # #
        # #
        # #     print('AMK:::::', scroll_to_view)
        # #     self.list_view.children[0].scroll_to(scroll_to_view, padding=30, animate=False)
        # # else:
        scroll_to_view = self.list_adapter.get_view(value)
        self.list_view.scroll_to(value)
            #if value > self.displayable:

            # from_bottom = (len(self.items) - len(self.list_view.container.children))
            # _value = value
            # if value > from_bottom:
            #     _value = from_bottom
            # print('VALUE: ', value, '_VALUE: ', _value)
        #new_view = self.list_adapter.get_view(value)

        if scroll_to_view:
            print('select:', scroll_to_view, 'for ', value)
            # self.list_view.children[0].scroll_to(scroll_to_view, padding=30, animate=True)
            #self.list_view.children[0].update_from_scroll()
            self.list_adapter.select_list([scroll_to_view])
        else:
            print('no view to scroll to!!!!!!!!!!!!!!!!')


    def on_path(self, instance, value):
        # clear the foo

        if self.list_adapter is None:
            args_converter = lambda row_index, rec: {'text': (rec.name + (' > ' if rec.is_dir() else '')),
                                                     'size_hint_y': None,
                                                     'height': 25,
                                                     'file': rec,
                                                     'halign': 'left',
                                                     # 'selected_color': [26,26,26,.2],
                                                     # 'deselected_color': [26,26,26,0],
                                                    }
            self.list_adapter = ListAdapter(
                   data=self.items,
                   args_converter=args_converter,
                   cls=CustomListItemButton,
                   allow_empty_selection=False
            )
            self.items = []
            # self.list_adapter.bind(on_selection_change=self.selection_changed)
            self.list_view = ListView(
                adapter=self.list_adapter
            )
            self.ids.container.add_widget(self.list_view)
        else:
            self.items = []


        for entry in scandir(self.path):
            if entry.is_dir():
                print('add direntry: ', entry.name)
                num_files = 0
                for _f in scandir(entry.path):
                    num_files += 1
                if num_files > 0:
                    self.items.append(entry)
                    self.list_adapter.data = self.items
            else:
                if entry.name.split(".")[-1] in SUPPORTED_FILES:
                    print('add filenetry: ', entry.name)
                    self.items.append(entry)
                    self.list_adapter.data = self.items

        self.list_adapter.data = self.items
        self.selection = 0
        return True
