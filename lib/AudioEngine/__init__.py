from kivy.core.audio import SoundLoader
from kivy.properties import ObjectProperty, NumericProperty
from kivy.uix.widget import Widget
from kivy.uix.image import Image
import librosa
import librosa.display
from kivy.garden.matplotlib.backend_kivyagg import FigureCanvasKivyAgg
import matplotlib.pyplot as plt

class AudioEngine(Widget):
    """docstring for AudioEngine."""
    sound = ObjectProperty(None)
    lr_sound = ObjectProperty(None, force_dispatch=True)
    sample_rate = NumericProperty(None)
    def __init__(self, arg):
        super(AudioEngine, self).__init__()
        self.register_event_type('on_sound_load')
        self.arg = arg
    def on_sound_load(self, event):
        pass

    def play(self, path, single=True):
        if single and (self.sound != None):
            self.sound.stop()
            self.sound.unload()
        self.sound = SoundLoader.load(path)
        y, sr = librosa.load(path, duration=self.sound.length, mono=False)
        self.lr_sound = y
        self.sample_rate = sr
        if self.sound:
            self.dispatch('on_sound_load', self.sound)
            self.sound.play()

    def get_waveform(self, sound):
        plt.style.use('dark_background')
        print(plt.style.available)
        plt.subplot(1, 1, 1);
        print(self.lr_sound, self.sample_rate);
        plt_collection = librosa.display.waveplot(self.lr_sound, sr=self.sample_rate, max_points=1000)
        print(plt_collection)
        plt_collection.set_color('0.1')

        plt.axis('off')
        plt.tight_layout()
        print(plt.gcf())

        return FigureCanvasKivyAgg(plt.gcf())
        # return Image()
